# ZAB Trinity Sector File

This repo contains the source code for the VATSIM Albuquerque ARTCC's sector file, codename Trinity.

# Adding ASDEX Colors to VRC

1. Start VRC
2. On the profile picker, at the top, choose `Tools` > `Import Color Profiles...`
3. Browse to this folder, and choose `asdex_colors.ini`
4. Select `ASDEX DAY` and `ASDEX NIGHT` and click `Import`

# License

Trinity is hereby released under the Creative Commons CC BY-NC-SA 4.0 license. Please ensure you are familiar with the license before contributing to this project. A couple of key takeaways:

1. If you choose to share or alter this project, you **MUST** give credit to the contributors of this project.
2. You may **NOT** use any of this project for commercial purposes.
3. If you create a derivitive of this project, that project **MUST** be released under the same license.

https://creativecommons.org/licenses/by-nc-sa/4.0/

# Contributors
The following people have contributed to this project. If you have contributed, but your name is not listed here, or if you are on this list and would like to be removed, please email atm@zabartcc.org.

Nate Johns  
Ed Sterling  
Chris Knepper  
Daan Janssen  
Austin Robison  
Michael Graham  

Thank you to all contributors, past and present.